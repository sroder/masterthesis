#Import surprise library functions
from surprise import SVD,NMF,SVDpp,evaluate
from surprise.dataset import Dataset
from surprise import Reader
from surprise.model_selection import cross_validate
from surprise.model_selection.search import GridSearchCV

# READ DATA
import pandas as pd
dfs = pd.read_excel('/data/OnlineRetail.xlsx', sheet_name='OnlineRetail')

#UNIQUE Values 
#exploring the unique values of each attribute
print("Number of transactions: ", dfs['InvoiceNo'].nunique())
print("Number of products bought: ",dfs['StockCode'].nunique())
print("Number of customers:", dfs['CustomerID'].nunique() )
print("Percentage of customers NA: ", round(dfs['CustomerID'].isnull().sum() * 100 / len(dfs),2),"%" )
print('Number of countries: ',dfs['Country'].nunique())

#remove canceled orders
dfs = dfs[dfs['Quantity']>0]

#remove rows where customerID are NA
dfs.dropna(subset=['CustomerID'],how='all',inplace=True)

###### Multiplying Quantity and UnitPrice columns to get a new column : AmountSpend########
dfs['AmountSpend'] = dfs['Quantity']*dfs['UnitPrice']

## Keeping relevant columns from the Database
matrix_setup = dfs[['StockCode','CustomerID','AmountSpend','InvoiceDate']]

#set all transations where amount was spend as 1
matrix_setup['AmountSpend'].loc[matrix_setup['AmountSpend']  > 0 ] = 1

# Rename the column AmountSpend as Itembought
matrix_setup = matrix_setup.rename(columns = {'AmountSpend':'ItemBought'})

# A list of all unique customers
customer_all = [i for i in matrix_setup['CustomerID']]
customer_unique = set(customer_all)
print len(customer_unique)," #unique customers"
print len(customer_all)," #all customers"

# A list of all unique items
items_all = [i for i in matrix_setup['StockCode']]
items_unique = set(items_all)
print len(items_unique)," #unique customers"
print len(items_all)," #all customers"

# creating user and item tuples
tuple_x = [list(zip(matrix_setup.CustomerID,matrix_setup.StockCode))]

# create negative rows
# rows which depicts that item was not bought
import random
neg_rows = []
for i in customer_unique:
    counter = 0
    shuffled_stock = list(items_unique)
    random.shuffle(shuffled_stock)
    for j in shuffled_stock:
        if counter == 20:
            break
        elif (i,j) not in tuple_x:
            row = (j,i)
            neg_rows.append(row)
            counter = counter + 1
			
# Intializing a dataframe to be used for negative rows
columns=['StockCode','CustomerID','ItemBought','InvoiceDate']
df3 = pd.DataFrame(columns=columns)

# creating a random time stamp list to be used in next step
Invoice_list = [i for i in matrix_setup['InvoiceDate']] 
random.shuffle(Invoice_list)

#Creating the itembought = 0 transaction dataframe
count = len(matrix_setup['StockCode'])
for i in range(len(neg_rows)):
    row = pd.Series({'StockCode':neg_rows[i][0],'CustomerID':neg_rows[i][1],'ItemBought':0,'InvoiceDate':Invoice_list[i]} )
    df3 = df3.append(row,ignore_index=True)
	
# Merging the two Dataframe
matrix_setup = matrix_setup.append(df3)
#sorting based on timestamp
matrix_setup = matrix_setup.sort_values('InvoiceDate')

# Zero has been set to 0.00001. 
#matrix_setup['ItemBought'].loc[matrix_setup['ItemBought']  == 0 ] = 0.00001
lower_bound = min(matrix_setup['ItemBought'])
upper_bound = max(matrix_setup['ItemBought'])

#define the reader  with  upper and lower bounds , 
reader_x = Reader(rating_scale = (lower_bound,upper_bound))



# for 90:10 distribution of the dataset
# find the dataframe index number which marks 90 percent of dataset
mark_90 = (matrix_setup.shape[0]/100)*90
# refereshing the index of dataset. This is required because after deletion of few records, index has missing entities
matrix_setup = matrix_setup.reset_index(drop=True)

# Splitting the dataset based on Timestamp for Traing and Testing
dfx_90 = matrix_setup[0:mark_90]
test_10 = matrix_setup[mark_90+1:]
print "Toatl records in Training: ",len(dfx_90)
neg_dfx = [i  for i in dfx_90['ItemBought'] if i < 1]
print "Total Not_Bought records in Training ",len(neg_dfx)
print "Percentage %",float(len(neg_dfx)*100/len(dfx_90))

#loading the data
data = Dataset.load_from_df(df=dfx_90[['CustomerID','StockCode','ItemBought']],reader=reader_x)
print 'difference in processed and pre-processed dataset = ',(data.raw_ratings[0][2] - data.df['ItemBought'][0])


# Grid Search
import time
start_time = time.time()
param_grid = {'n_factors':[2,5,50],'n_epochs': [5,10,50], 'reg_pu': [0.1,0.01,0.001],'reg_qi': [0.1,0.01,0.001]}
grid_search = GridSearchCV(NMF, param_grid, measures=['rmse', 'mae'], cv=3, n_jobs=1)
grid_search.fit(data)

print 'best RMSE score'
print(grid_search.best_score['rmse'])
print 'combination of parameters that gave the best RMSE score'
print(grid_search.best_params['rmse'])
print 'best MAE score'
print(grid_search.best_score['mae'])
print 'combination of parameters that gave the best MAE score'
print(grid_search.best_params['mae'])
print("--- %s seconds for GridSearch---" % (time.time() - start_time))

results_df = pd.DataFrame.from_dict(grid_search.cv_results)

writer = pd.ExcelWriter('IB_NMF_all_GS_TS_90.xlsx')
results_df.to_excel(writer,'Sheet1')
writer.save()

