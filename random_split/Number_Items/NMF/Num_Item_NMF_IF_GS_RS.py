import pandas as pd
import seaborn as sns
import numpy as np
from matplotlib import pyplot as plt
#%matplotlib inline
plt.rcParams["figure.figsize"] = [16,9]
from surprise import SVD,NMF,SVDpp,evaluate
from surprise.dataset import Dataset
from surprise import Reader
from surprise.model_selection import cross_validate
from surprise.dataset import DatasetAutoFolds
from surprise.model_selection.search import GridSearchCV
from surprise.model_selection import train_test_split
from surprise.model_selection.search import GridSearchCV

# READ DATA and pre-process
dfs = pd.read_excel('/data/OnlineRetail.xlsx', sheet_name='OnlineRetail')

#removing cancelled orders
dfs = dfs[dfs['Quantity']>0]

#remove rows where customerID are NA
dfs.dropna(subset=['CustomerID'],how='all',inplace=True)

###### Multiplying Quantity and UnitPrice columns to get a new column : AmountSpend########
dfs['AmountSpend'] = dfs['Quantity']*dfs['UnitPrice']

## Keeping relevant coluns from the Database
matrix_setup = dfs[['StockCode','CustomerID','Quantity']]
# removing outliers
matrix_setup = matrix_setup[(matrix_setup['Quantity'] < 5000)]

lower_bound = min(matrix_setup['Quantity'])
upper_bound = max(matrix_setup['Quantity'])
#Normalization
matrix_setup['Norm_Quantity']= (matrix_setup['Quantity'] -lower_bound)/(upper_bound -lower_bound)

#removing more outliers
dfx=matrix_setup[matrix_setup['Norm_Quantity'] <= 0.4]
#dfx.loc[dfx['Norm_Quantity'] == 0, 'Norm_Quantity'] = 0.000001
lower_bound = min(dfx['Norm_Quantity'])
upper_bound = max(dfx['Norm_Quantity'])
print 'Upper bound is ',upper_bound

#define the reader  with  upper and lower bounds , also now we are predicting Normalized Total Amount column
reader_x = Reader(rating_scale = (lower_bound,upper_bound))

data = Dataset.load_from_df(df=dfx[['CustomerID','StockCode','Norm_Quantity']],reader=reader_x)


print 'difference in processed and pre-processed dataset = ',(data.raw_ratings[0][2] - data.df['Norm_Quantity'][0])


import time
start_time = time.time()

param_grid = {'n_factors':[2,5,50],'n_epochs': [5,10,50], 'reg_pu': [0.1,0.01,0.001],'reg_qi': [0.1,0.01,0.001]}
grid_search = GridSearchCV(NMF, param_grid, measures=['rmse', 'mae'], cv=3, n_jobs=1)

grid_search.fit(data)

print 'best RMSE score'
print(grid_search.best_score['rmse'])

print 'combination of parameters that gave the best RMSE score'
print(grid_search.best_params['rmse'])

print 'best MAE score'
print(grid_search.best_score['mae'])

print 'combination of parameters that gave the best MAE score'
print(grid_search.best_params['mae'])

print("--- %s seconds for GridSearch---" % (time.time() - start_time))

results_df = pd.DataFrame.from_dict(grid_search.cv_results)
writer = pd.ExcelWriter('Num_Item_NMF_GS_RS.xlsx')
results_df.to_excel(writer,'Sheet1')
writer.save()
